
import unittest

# Define the 'calc' function here
def calc(string):
    
    string = string.split()
    
    if len(string) == 1:
        return int(string[0])
    
    r = 0
    for i in xrange(0, len(string)-2):   
        if string[i].isdigit() and string[i+2].isdigit() and string[i+1] == "PLUS":
            r = int(string[i]) + int(string[i+2])
            string[i+2] = str(r)
        if string[i].isdigit() and string[i+2].isdigit() and string[i+1] == "MINUS":
            r = int(string[i]) - int(string[i+2])
            string[i+2] = str(r)
    return r
    

class TestCalculator(unittest.TestCase):
    def test_number(self):
        self.assertEqual(calc("5"), 5)

    def test_simple_expression(self):
        self.assertEqual(calc("1 PLUS 2"), 3)

    def test_complex_expression(self):
        self.assertEqual(calc("2 PLUS 5 MINUS 1 PLUS 4"), 10)

unittest.main()