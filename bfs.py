from collections import deque

def bfs(A, startX, startY):
	if A == []:
		return "Empty graph"

	queue = deque([])
	color = A[startX][startY]

	queue.append((startX, startY))

	while queue:
		
		i, j = queue.popleft()
		
		if (j < len(A[0]) - 1 and  A[i][j+1] == color and A[i][j+1] != None):
			queue.append((i,j+1))

		if (i < len(A) - 1 and A[i+1][j] == color and A[i+1][j] != None):
			queue.append((i+1,j))

		if (j > 0 and A[i][j-1] == color and A[i][j-1] != None):
			queue.append((i,j-1))
		
		if (i > 0 and A[i-1][j] == color and A[i-1][j] != None):
			queue.append((i-1,j))

		A[i][j] = None

	print A

if __name__ == '__main__':
	# A = [[1, 2], [1, 0], [0, 0], [1,1]]
	A = [[1,1], [1,1]]
	
	#count the number of colors/regions

	count = 0
	for i in xrange(0, len(A)):
		for j in xrange(0, len(A[0])):
			if A[i][j] != None:
				bfs(A, i, j)
				count = count + 1

	print count

