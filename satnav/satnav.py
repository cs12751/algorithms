from copy import deepcopy
from collections import deque

class SatNav(object):
	"""
	The Sat Nav system is to guide a driver around a network of one way streets in a small town. 
	The road junctions are inspiringly named A to E. The map data provides you with the distance between 
	these junctions, as well as the direction of travel that is permitted along that street. 
	For example AB4 represents a road from A to B (but not from B to A), with a length of 4.

	Class provides code that calculates the length of a given route, the number of possible 
	routes between two points (that meet some conditions), and the shortest route between two points. 
	"""
	graph = {}

	def __init__(self, data):
		super(SatNav, self).__init__()
		self.graph = self.genGraph(data)

	def getGraph(self):
		return self.graph
	
	def genGraph(self, string):
		graph = {}
		array = sorted(string.split(","))
		
		entries = []
		for entry in array:
			entries.append(list(entry))

		for n1, n2, d in entries:
			graph.setdefault(n1, {})[n2] = d

		return graph

	def dijkstra(self, src, end):
		dist = {}
		pred = {}
		Q    = {}

		#if start equals end node, we create an additional node. 
		if src == end:
			#create copy of the graph, with a different memory location
			graph_copy = deepcopy(self.graph)

			graph_copy[src+"P"] = graph_copy[src]
			for node in self.graph:
				for n in self.graph[node]:
					if n == src:
						graph_copy[node][src+"P"] = graph_copy[node][src]

		#create a pointer to which version of the graph we are using
		graph = {}
		if src == end:
			graph = graph_copy
		else:
			graph = self.graph

		#set all other distances to infinity
		for v in graph:
			if v != src:
				dist[v] = float('inf') 
				pred[v] = None
				Q[v] = {} #put v in Queue
			else:
				dist[src] = 0
				Q[src] = 0

		while Q: 
			u = min(Q, key=Q.get) #vertex in Q with min dist[u]
			del Q[u] #remove from queue
			
			for v in graph[u]: #for all neighbors of extracted vertex
				
				alt = dist[u] + int(graph[u][v]) #calculate distance
				
				if alt < dist[v]: #shorter path found
					dist[v] = alt
					pred[v] = u 

		print dist
		#if there is no path
		if dist[end] == float('inf'):
			#raise Exception("No such path in the graph")
			return -1
		elif src == end:
			return dist[end+'P']
		else:
			return dist[end]

	def calcMultiple(self, *args):
		distance = 0
		nodes = []
		for i in xrange(0, len(args) - 1):
			if self.dijkstra == -1:
				raise Exception
			distance += self.dijkstra(args[i], args[i+1])
		
		return distance

	def test10(self, start, end, limit):
		graph, t = self.alterGraph(start, end)
		return self.kShortestPath(start, t, limit, graph)

	def test6(self, start, end, limit):
		graph, t = self.alterGraph(start, end)
		graph = self.unweightGraph(graph)
		return self.kShortestPath(start, t, limit + 1, graph)

	def kShortestPath(self, src, end, limit, graph):

		longest_path = 0 
		Q = {}
		p = [] #paths and respective lengths

		Q[src] = 0

		#graph, t = self.alterGraph(src, end)

		while Q and longest_path < limit:
			#print Q
			u = min(Q, key=Q.get)
			c = Q[u] #cost
			del Q[u] #remove
			
			last = u[-1] #last element of the path

			if last == end:	
				p.append(u)
				longest_path = c
			if longest_path <= limit:
				for v in graph[last]:
					if v != src:
						alt = u + v
						Q[alt] = c + int(graph[last][v])
		
		return len(p)-1

	def alterGraph(self, src, end):
		t = end
		if src == end:
			t = "*"
			#create copy of the graph, with a different memory location
			graph_copy = deepcopy(self.graph)

			graph_copy["*"] = graph_copy[src]
			for node in self.graph:
				for n in self.graph[node]:
					if n == src:
						graph_copy[node]["*"] = graph_copy[node][src]

		graph = {}
		if src == end:
			graph = graph_copy
		else:
			graph = self.graph

		return graph, t

	def unweightGraph(self, graph_copy):

		#graph_copy = deepcopy(graph)

		for node in graph_copy:
			for element in graph_copy[node]:
				graph_copy[node][element] = 1

		return graph_copy
