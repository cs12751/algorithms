import unittest
from satnav import SatNav

data = "AB5,BC4,CD7,DC8,DE6,AD5,CE2,EB3,AE7"
sn = SatNav(data)

class TestSatNav(unittest.TestCase):

	def test1(self):
		self.assertEqual(sn.calcMultiple("A", "B", "C"), 9)

	def test2(self):
		self.assertEqual(sn.calcMultiple("A", "D"), 5)

	def test3(self):
		self.assertEqual(sn.calcMultiple("A", "D", "C"), 13)

	def test4(self):
		self.assertEqual(sn.calcMultiple("A", "E", "B", "C", "D"), 21)

	def test5(self):
		self.assertRaises(Exception, sn.calcMultiple("A", "E", "D"))

	def test6(self):
		self.assertEqual(sn.test6("C","C", 3),2)

	def test7(self):
		pass

	def test8(self):
		self.assertEqual(sn.dijkstra("A","C"),9)

	def test9(self):
		self.assertEqual(sn.dijkstra("B","B"),9)

	def test10(self):
		self.assertEqual(sn.test10("C","C",30),9)

unittest.main()