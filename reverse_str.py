def reverse(string):

	string = string.split()

	tmp = ""
	n = len(string) - 1

	for i in xrange(0, len(string)/2):
		
		tmp = string[i]
		string[i] = string[n]
		string[n] = tmp
		n = n - 1

	print string

if __name__ == '__main__':
	
	string = "a b c d"

	reverse(string)