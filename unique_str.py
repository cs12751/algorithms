def isUnique(string):
	string = string.split()
	check  = {}

	#print string

	for e in string:
		if e in check:
			check[e] += 1
		else:
			check[e] = 1
	#print check

	for e in check:
		if check[e] != 1:
			return False
	return True

if __name__ == '__main__':
	s = "a b c d"
	s1 = "a b c a"
	print(isUnique(s))