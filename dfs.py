

def solve(A, startX, startY):
	stack = []
	color = A[startX][startY] 

	stack.append((startX, startY))

	while stack != []:
		i,j = stack.pop() 

		if (j < len(A[0]) -1 and A[i][j+1] != None and A[i][j+1] == color):
			stack.append((i,j+1))
		if (i < len(A) - 1 and A[i+1][j] != None and A[i+1][j] == color):
			stack.append((i+1,j))
		if (i > 0 and A[i-1][j] != None and A[i-1][j] == color):
			stack.append((i-1,j))
		if (j > 0 and A[i][j-1] != None and A[i][j-1] == color):
			stack.append((i,j-1))

		A[i][j] = None

	print A

if __name__ == '__main__':
	A = [[1, 1], [1, 0]]
	solve(A, 1, 0)